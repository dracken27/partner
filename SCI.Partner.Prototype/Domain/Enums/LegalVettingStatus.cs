﻿namespace Domain.Enums
{
    public enum LegalVettingStatus
    {
        NotVetted = 0,
        AwaitingLegalVetting = 1,
        Debarred = 2,
        Vetted = 3
    }
}
