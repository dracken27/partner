namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialSetup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Partners",
                c => new
                    {
                        PartnerId = c.Int(nullable: false, identity: true),
                        PartnerName = c.String(),
                        PartnerAcronim = c.String(),
                        Location = c.String(),
                        Code = c.String(),
                        Address = c.String(),
                        PartnerTypeId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Archive = c.Boolean(nullable: false),
                        LegalVettingStatus = c.Int(nullable: false),
                        VettedBy = c.String(),
                        StatusOfOrganisationalAssessment = c.Int(nullable: false),
                        DateOfAssesment = c.DateTime(),
                        ContactPerson = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.PartnerId)
                .ForeignKey("dbo.PartnerTypes", t => t.PartnerTypeId, cascadeDelete: true)
                .Index(t => t.PartnerTypeId);
            
            CreateTable(
                "dbo.PartnerTypes",
                c => new
                    {
                        PartnerTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.PartnerTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Partners", "PartnerTypeId", "dbo.PartnerTypes");
            DropIndex("dbo.Partners", new[] { "PartnerTypeId" });
            DropTable("dbo.PartnerTypes");
            DropTable("dbo.Partners");
        }
    }
}
