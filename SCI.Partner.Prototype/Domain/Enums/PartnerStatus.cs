﻿namespace Domain.Enums
{
    public enum PartnerStatus
    {
        Draft = 0,
        Debarred = 1,
        Active = 2,
        Archived = 3
    }
}
