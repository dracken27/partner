using Domain.Lookups;
using System.Data.Entity.Migrations;

namespace Data.Migrations
{

    internal sealed class Configuration : DbMigrationsConfiguration<PartnerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PartnerContext context)
        {
            context.PartnerTypes.AddOrUpdate(
                new PartnerType { Name = "Sub-national government/authorities" },
                new PartnerType { Name = "National government/authorities" },
                new PartnerType { Name = "Local / national NGO" },
                new PartnerType { Name = "Community based organisation /group" },
                new PartnerType { Name = "Child-youth led organisation / group" },
                new PartnerType { Name = "Membership based organisation" },
                new PartnerType { Name = "Network/Coalition/Movement" },
                new PartnerType { Name = "International NGO" },
                new PartnerType { Name = "Interstate organisation" },
                new PartnerType { Name = "Private sector/ Corporate" },
                new PartnerType { Name = "UN agency" },
                new PartnerType { Name = "Academic/research institution" });
        }
    }
}
