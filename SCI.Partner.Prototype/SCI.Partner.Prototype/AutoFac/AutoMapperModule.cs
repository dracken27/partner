﻿using Autofac;
using AutoMapper;
using Mapper = SCI.Partner.Prototype.AutoMapper.Mapper;

namespace SCI.Partner.Prototype.AutoFac
{
    public class AutoMapperModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(Mapper.CreateMapper()).As<IMapper>().SingleInstance();
        }
    }
}