﻿using Autofac;
using Data;

namespace SCI.Partner.Prototype.AutoFac
{
    public class EntityFrameworkModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PartnerContext>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().InstancePerRequest();
        }
    }
}