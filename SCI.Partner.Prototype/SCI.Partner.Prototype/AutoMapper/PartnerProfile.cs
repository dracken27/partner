﻿using AutoMapper;
using Manager.Model;

namespace SCI.Partner.Prototype.AutoMapper
{
    public class PartnerProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Domain.Partner, PartnerInfo>();
            CreateMap<PartnerInput, Domain.Partner>();
        }
    }
}