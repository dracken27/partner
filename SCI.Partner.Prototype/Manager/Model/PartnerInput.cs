﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace Manager.Model
{
    public class PartnerInput
    {
        public int PartnerId { get; set; }
        
        [Required]
        public string PartnerName { get; set; }
        
        public string PartnerAcronym { get; set; }
        
        [Required]
        public string Location { get; set; }
        
        public string Address { get; set; }
        
        [Required]
        public int PartnerTypeId { get; set; }
        
        public bool Archive { get; set; }
        
        public StatusOfOrganisationalAssessment StatusOfOrganisationalAssessment { get; set; }

        public DateTime? DateOfAssesment { get; set; }

        public string ContactPerson { get; set; }

        public string Description { get; set; }
    }
}
