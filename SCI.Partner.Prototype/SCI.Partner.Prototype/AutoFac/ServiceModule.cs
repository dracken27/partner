﻿using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace SCI.Partner.Prototype.AutoFac
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var managerAssembly = Assembly.Load("Manager");
            builder.RegisterAssemblyTypes(managerAssembly).Where(t => t.Name.EndsWith("Service"));
        }
    }
}