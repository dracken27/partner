﻿using Contracts;
using Domain;

namespace Data.Repositories
{
    public class PartnerRepository : Repository<Partner>, IPartnerRepository
    {
        public PartnerRepository(PartnerContext context) : base(context)
        {
        }
    }
}
