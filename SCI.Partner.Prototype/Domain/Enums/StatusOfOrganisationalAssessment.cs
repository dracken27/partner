﻿namespace Domain.Enums
{
    public enum StatusOfOrganisationalAssessment
    {
        Assessed = 0,
        Debarred = 1,
        NotAssessed = 2
    }
}
