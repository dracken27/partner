﻿using System;

namespace Data
{
    public class UnitOfWork : IDisposable
    {
        private PartnerContext _context;

        public UnitOfWork(PartnerContext context)
        {
            _context = context;
        }

        public int Comit()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing || _context == null) return;

            _context.Dispose();
            _context = null;
        }
    }
}
