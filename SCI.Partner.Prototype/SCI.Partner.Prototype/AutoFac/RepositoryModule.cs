﻿using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace SCI.Partner.Prototype.AutoFac
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var dataAssembly = Assembly.Load("Data");
            builder.RegisterAssemblyTypes(dataAssembly).Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces();
        }
    }
}