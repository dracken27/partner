﻿using System.Data.Entity;
using Domain;
using Domain.Lookups;

namespace Data
{
    public class PartnerContext : DbContext
    {
        public PartnerContext() : base("Partner") { }

        public DbSet<Partner> Partners { get; set; }
        public DbSet<PartnerType> PartnerTypes { get; set; }
    }
}
