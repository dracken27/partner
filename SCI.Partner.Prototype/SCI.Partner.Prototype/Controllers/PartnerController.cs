﻿using System.Web.Http;
using Manager.Model;
using Manager.Service;

namespace SCI.Partner.Prototype.Controllers
{
    [RoutePrefix("partner")]
    public class PartnerController : ApiController
    {
        private readonly PartnerService _partnerService;

        public PartnerController(PartnerService partnerService)
        {
            _partnerService = partnerService;
        }

        [Route]
        public IHttpActionResult GetPartners()
        {
            var partners = _partnerService.GetAllPartners();
            return Ok(partners);
        }

        [Route("{id}")]
        public IHttpActionResult GetPartner(int id)
        {
            var partner = _partnerService.GetPartner(id);
            return Ok(partner);
        }

        [Route]
        [HttpPost]
        public IHttpActionResult Add(PartnerInput partnerInput)
        {
            _partnerService.AddPartner(partnerInput);
            return Ok();
        }

        [Route]
        [HttpPut]
        public IHttpActionResult Update(PartnerInput partnerInput)
        {
            _partnerService.UpdatePartner(partnerInput);
            return Ok();
        }

        [Route("{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var succesful = _partnerService.DeletePartner(id);

            if (succesful)
                return Ok();
            return NotFound();
        }
    }
}
