﻿using AutoMapper;

namespace SCI.Partner.Prototype.AutoMapper
{
    public static class Mapper
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<PartnerProfile>());

            return config.CreateMapper();
        }
    }
}