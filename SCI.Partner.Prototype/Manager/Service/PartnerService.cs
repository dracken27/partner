﻿using System.Collections.Generic;
using AutoMapper;
using Contracts;
using Data;
using Domain;
using Manager.Model;

namespace Manager.Service
{
    public class PartnerService
    {
        private readonly IPartnerRepository _partnerRepository;
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PartnerService(IPartnerRepository partnerRepository, UnitOfWork unitOfWork, IMapper mapper)
        {
            _partnerRepository = partnerRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<PartnerInfo> GetAllPartners()
        {
            var partnersList = _partnerRepository.GetAll();
            return _mapper.Map<IList<PartnerInfo>>(partnersList);
        }

        public PartnerInfo GetPartner(int id)
        {
            var partner = _partnerRepository.GetById(id);
            return _mapper.Map<PartnerInfo>(partner);
        }

        public void AddPartner(PartnerInput partnerInput)
        {
            var partner = _mapper.Map<Partner>(partnerInput);

            _partnerRepository.Add(partner);

            _unitOfWork.Comit();
        }

        public void UpdatePartner(PartnerInput partnerInput)
        {
            var partner = _partnerRepository.GetById(partnerInput.PartnerId);
            _mapper.Map(partnerInput, partner);

            _partnerRepository.Update(partner);

            _unitOfWork.Comit();
        }

        public bool DeletePartner(int id)
        {
            var partner = _partnerRepository.GetById(id);

            if (partner == null)
                return false;

            _partnerRepository.Delete(partner);

            _unitOfWork.Comit();

            return true;
        }
    }
}
