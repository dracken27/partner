﻿namespace Domain.Lookups
{
    public class PartnerType
    {
        public int PartnerTypeId { get; set; }

        public string Name { get; set; }
    }
}
