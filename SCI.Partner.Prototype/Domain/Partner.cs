﻿using System;
using Domain.Enums;
using Domain.Lookups;

namespace Domain
{
    public class Partner
    {
        public int PartnerId { get; set; }

        public string PartnerName { get; set; }

        public string PartnerAcronim { get; set; }

        public string Location { get; set; }

        public string Code { get; set; }

        public string Address { get; set; }

        public int PartnerTypeId { get; set; }

        public virtual PartnerType PartnerType { get; set; }

        public PartnerStatus Status { get; set; }

        public bool Archive { get; set; }

        public LegalVettingStatus LegalVettingStatus { get; set; }

        public string VettedBy { get; set; }

        public StatusOfOrganisationalAssessment StatusOfOrganisationalAssessment { get; set; }

        public DateTime? DateOfAssesment { get; set; }

        public string ContactPerson { get; set; }

        public string Description { get; set; }
    }
}
