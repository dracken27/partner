﻿using Domain;

namespace Contracts
{
    public interface IPartnerRepository : IRepository<Partner>
    {
    }
}
