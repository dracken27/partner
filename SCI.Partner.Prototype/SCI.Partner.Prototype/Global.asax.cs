﻿using System.Web.Http;
using System.Web.Mvc;
using SCI.Partner.Prototype.AutoFac;

namespace SCI.Partner.Prototype
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoFacConfig.RegisterTypes();
        }
    }
}
